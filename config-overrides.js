/**
 * Override webpack config to work with index.php file
 */
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (config, env) => {
    const isEnvProduction = env === 'production';

    const htmlPlugin = new HtmlWebpackPlugin(
        Object.assign(
            {},
            {
                inject: true,
                template: 'public/index.php',
                filename: 'index.php',
                alwaysWriteToDisk: true
            },
            isEnvProduction
                ? {
                      minify: {
                          removeComments: true,
                          collapseWhitespace: true,
                          removeRedundantAttributes: true,
                          useShortDoctype: true,
                          removeEmptyAttributes: true,
                          removeStyleLinkTypeAttributes: true,
                          keepClosingSlash: true,
                          minifyJS: true,
                          minifyCSS: true,
                          minifyURLs: true
                      }
                  }
                : undefined
        )
    );

    // replace html plugin
    config.plugins[1] = htmlPlugin;

    // find fileLoader and exclude php file
    const rulesLength = config.module.rules[2].oneOf.length;
    const fileLoader = config.module.rules[2].oneOf[rulesLength - 1];

    fileLoader.exclude.push(/\.php$/);

    return config;
};
