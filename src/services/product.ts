import axios from './axios';

export const get = (data: Viewer.GetProductByIdRequest) =>
    axios<Viewer.ProductResponse>({
        url: '/product/get',
        data
    });
