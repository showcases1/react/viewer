import * as playlist from './playlist';
import * as product from './product';
import * as user from './user';
import * as video from './video';

export const API = {
    user,
    playlist,
    product,
    video
};
