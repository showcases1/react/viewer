import axios, { AxiosPromise, AxiosRequestConfig } from 'axios';
import { state } from '../effects/user';

const ViewerAxiosInstance = axios.create();

ViewerAxiosInstance.defaults.baseURL = 'https://viewer.com/';
ViewerAxiosInstance.defaults.method = 'POST';
ViewerAxiosInstance.interceptors.response.use(
    config => config.data,
    config => Promise.reject(config.response.data)
);

export default <T = void>(config: AxiosRequestConfig, withToken = true) => {
    const request: AxiosPromise<T> = ViewerAxiosInstance({
        ...config,
        headers: withToken
            ? {
                  Authorization: `Bearer ${state.user.getState().token}`
              }
            : {}
    });

    return (request as any) as Promise<T>;
};
