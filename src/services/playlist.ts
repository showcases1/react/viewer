import axios from './axios';

export const query = (data: Viewer.PlaylistVideosRequest) =>
    axios<Viewer.PlaylistVideosResponse>({
        url: '/playlist/query',
        data
    });

export const get = (data: Viewer.GetPlaylistVideoRequest) =>
    axios<Viewer.PlaylistSingleVideoResponse>({
        url: '/playlist/get',
        data
    });
