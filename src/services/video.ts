import axios from './axios';

export const get = (data: Viewer.RegisterAggregatedEventRequest) =>
    axios({
        url: '/video/register-event',
        data
    });

export const queryCreator = (data: Viewer.QueryCreatorVideosRequest) =>
    axios<Viewer.QueryCreatorVideosResponse>({
        url: '/playlist/query-by-creator',
        data
    });
