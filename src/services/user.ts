import axios from './axios';

export const createAccountAnonymous = (data: Viewer.UserCreateAnonymousAccountRequest) =>
    axios<Viewer.UserJwtTokenResponse>(
        {
            url: '/user/create-account-anonymous',
            data
        },
        false
    );
