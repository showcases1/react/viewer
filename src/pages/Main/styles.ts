import styled, { css } from 'styled-components';
import { Video } from '../Video';

interface Props {
    isMobile: boolean;
}

export const VideoStyled = styled(Video)`
    padding-top: 38px;
    padding-bottom: 38px;
`;

export const MainStyled = styled.div<Props>`
    overflow: hidden;
    height: 100vh; /* Use vh as a fallback for browsers that do not support Custom Properties */
    height: calc(var(--vh, 1vh) * 100);

    ${p =>
        p.isMobile &&
        css`
            display: flex;
            align-items: center;
            background-color: black;

            ${VideoStyled} {
                padding-bottom: 0;
                padding-top: 0;
            }
        `}
`;
