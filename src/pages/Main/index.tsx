import React, { useEffect } from 'react';
import { isMobile } from 'react-device-detect';
import { Header } from '../../components/common/Header';
import { MainStyled, VideoStyled } from './styles';

export const Main: React.FC = () => {
    useEffect(() => {
        const vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    }, []);

    return (
        <MainStyled isMobile={isMobile}>
            <Header />
            <VideoStyled />
        </MainStyled>
    );
};
