import LoadingImg from 'assets/img/load.gif';
import { ModalProducts } from 'components/common/ModalProducts';
import { RelatedVideo } from 'components/common/RelatedVideo';
import { SliderVideo } from 'components/common/SliderVideo';
import { Video as VideoPlayer } from 'components/common/Video';
import { useStore } from 'effector-react';
import React, { FC, useCallback, useEffect, useState } from 'react';
import { isMobile } from 'react-device-detect';
import { useParams } from 'react-router';
import { useHistory } from 'react-router-dom';
import RotatePhone from '../../assets/img/rotate-phone.png';
import { effects, state } from '../../effects/playlist';
import { state as stateUser } from '../../effects/user';
import { effects as effectsVideo, state as stateVideo } from '../../effects/video';
import { withAdaptiveComponent } from '../../hoc/withAdaptiveComponent';
import { Content, Loading, Stub, StubDescription, StubImage, StubWrap, VideoStyled, Wrapper } from './styles';

const MAX_VIDEO_COUNT = 10;

interface VideoComponentProp {
    isShowProducts: boolean;
    isRelatedVideo: boolean;
    width: number;
    height: number;
    className?: string;
}

const VideoComponent: FC<VideoComponentProp> = ({ isShowProducts, isRelatedVideo, width, height, className }) => {
    const { id } = useParams();
    const video = useStore(state.video);
    const sessionId = useStore(stateUser.sessionId);
    const playlistItems = useStore(state.playlistItems);
    const playlistItemsCreator = useStore(stateVideo.playlistItemsCreator);
    const oldParamsVideo = useStore(stateVideo.oldParamsVideo);
    const paramsVideo = useStore(stateVideo.informationSeeVideo);
    const [isOpenPopup, setOpenPopup] = useState(false);
    const loadedVideo = useStore(state.loadedVideo);
    const videoIdCurrent = useStore(state.videoSlideId);
    const [isMuted, setMuted] = useState(true);
    const history = useHistory();

    const handleToggleMutedVideo = useCallback(() => {
        setMuted(!isMuted);
    }, [isMuted]);

    useEffect(() => {
        history.listen((_loc, action) => {
            if (action === 'POP') {
                window.location.reload();
            }
        });
    }, [history]);

    useEffect(() => {
        effects.mainLoad(id);
        effectsVideo.saveParamsVideo({
            id,
            sessionId
        });
    }, [id, sessionId]);

    const handleTogglePopup = useCallback(() => {
        setOpenPopup(!isOpenPopup);
    }, [isOpenPopup]);

    const handleClickRelatedVideo = () => {
        const date = new Date();
        const time = date.getTime();
        const percent = Number(((time - paramsVideo.time) / 1000 / paramsVideo.duration).toFixed(2));
        effectsVideo.registerEventVideo({
            videoId: oldParamsVideo.id,
            sessionId: oldParamsVideo.sessionId,
            viewDurationPercentage: percent > 1 ? 1 : percent
        });
    };

    const isItemsCreator = playlistItemsCreator.length > 0;
    const playlistItemsResult = isItemsCreator ? playlistItemsCreator : playlistItems;
    const videos = playlistItemsResult.filter(item => item.id !== video.id).slice(0, MAX_VIDEO_COUNT - 1);

    const videoList = videos.length > 0 ? [video, ...videos] : null;

    const filterVideoCurrentId = videoList?.filter(item => item.id === videoIdCurrent)[0];

    const videoResult = filterVideoCurrentId || video;

    return (
        <VideoStyled className={className} isMobile={isMobile} loadedVideo={loadedVideo}>
            <Stub>
                <StubWrap>
                    <StubImage alt="rotate-phone" src={RotatePhone} />
                    <StubDescription>Rotate your Phone for a better experience</StubDescription>
                </StubWrap>
            </Stub>
            <Content>
                <Wrapper height={height} width={width}>
                    {videoList && isMobile && (
                        <SliderVideo
                            isMuted={isMuted}
                            isShowInformation={!isShowProducts}
                            list={videoList}
                            onOpenPopup={handleTogglePopup}
                            onToggleMutedVideo={handleToggleMutedVideo}
                        />
                    )}
                    {!isMobile && (
                        <VideoPlayer
                            isActiveVideo
                            isMuted={isMuted}
                            isShowInformation={!isShowProducts}
                            video={video}
                            onOpenPopup={handleTogglePopup}
                            onToggleMutedVideo={handleToggleMutedVideo}
                        />
                    )}
                </Wrapper>
                {isShowProducts && (
                    <Wrapper height={height} width={width}>
                        <ModalProducts
                            staticState
                            isShow={isOpenPopup}
                            video={videoResult}
                            onTogglePopup={handleTogglePopup}
                        />
                    </Wrapper>
                )}
                {!isShowProducts && (
                    <ModalProducts isShow={isOpenPopup} video={videoResult} onTogglePopup={handleTogglePopup} />
                )}
                {isRelatedVideo && (
                    <Wrapper background="black" height={height} width={width}>
                        <RelatedVideo
                            isItemsCreator={isItemsCreator}
                            video={video}
                            videos={loadedVideo ? videos : []}
                            onClick={handleClickRelatedVideo}
                        />
                    </Wrapper>
                )}
            </Content>
            <Loading src={LoadingImg} />
        </VideoStyled>
    );
};

export const Video = withAdaptiveComponent(VideoComponent);
