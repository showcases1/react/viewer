import styled, { css } from 'styled-components';
import { colors } from 'variables';

export const Stub = styled.div`
    position: absolute;
    z-index: 1000;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
    display: none;
    background-color: rgba(0, 0, 0, 0.4);
`;

export const StubWrap = styled.div`
    text-align: center;
`;

export const StubImage = styled.img`
    margin-bottom: 12px;
    width: 70px;
    height: 70px;
`;

export const StubDescription = styled.p`
    margin: 0;
    font-size: 14px;
    font-weight: bold;
    max-width: 164px;
    letter-spacing: 0.6px;
    color: ${colors.white};
`;

export const Content = styled.div`
    width: 100%;
    height: 100%;
    display: none;
    align-items: center;
    justify-content: center;
`;

export const Loading = styled.img`
    position: absolute;
    top: 50%;
    left: 50%;
    width: 225px;
    height: 225px;
    transform: translate(-50%, -50%);
`;

interface WrapperProp {
    width: number;
    height: number;
    background?: string;
}

export const Wrapper = styled.div<WrapperProp>`
    position: relative;
    flex-shrink: 0;
    width: ${p => `${p.width}px`};
    height: ${p => `${p.height}px`};
    background: ${p => p.background};

    .swiper-container {
        height: 100%;
        width: 100%;
    }

    .swiper-container-vertical > .swiper-scrollbar {
        display: none;
    }
`;

interface VideoStyledProps {
    loadedVideo: boolean;
    isMobile: boolean;
}

export const VideoStyled = styled.div<VideoStyledProps>`
    position: relative;
    width: 100%;
    height: calc(100% - 62px);

    ${p =>
        p.loadedVideo &&
        css`
            ${Content} {
                display: flex;
                height: auto;
            }

            ${Loading} {
                display: none;
            }
        `}

    ${p =>
        p.isMobile &&
        css`
            height: auto;
            ${Stub} {
                @media (orientation: landscape) {
                    display: flex;
                }
            }

            ${Content} {
                @media (orientation: landscape) {
                    filter: blur(40px);
                }
            }

            ${Loading} {
                width: 200px;
                height: 200px;
            }
        `}
`;
