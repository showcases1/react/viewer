import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans',
        'Droid Sans', 'Helvetica Neue', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    overflow: hidden;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace;
  }

  * {
    box-sizing: border-box;
  }

  @font-face {
    font-family: 'SFProDisplay Regular';
    src: url('./fonts/SFProDisplay-Regular.woff2') format('woff2'), url('./fonts/SFProDisplay-Regular.woff') format('woff'),
        url('./fonts/SFProDisplay-Regular.ttf') format('truetype');
    font-weight: 400;
    font-style: normal;
  }

  @font-face {
      font-family: 'SFProDisplay SemiBold';
      src: url('./fonts/SFProDisplay-Semibold.woff2') format('woff2'), url('./fonts/SFProDisplay-Semibold.woff') format('woff'),
          url('./fonts/SFProDisplay-Semibold.ttf') format('truetype');
      font-weight: 600;
      font-style: normal;
  }

  @font-face {
      font-family: 'SFUIDisplay SemiBold';
      src: url('./fonts/sf-ui-display-semibold.woff2') format('woff2'), url('./fonts/sf-ui-display-semibold.woff') format('woff'),
          url('./fonts/sf-ui-display-semibold.ttf') format('truetype');
      font-weight: 600;
      font-style: normal;
  }

  @font-face {
      font-family: 'SFUIDisplay Bold';
      src: url('./fonts/sf-ui-display-bold.woff2') format('woff2'), url('./fonts/sf-ui-display-bold.woff') format('woff'),
          url('./fonts/sf-ui-display-bold.ttf') format('truetype');
      font-weight: 700;
      font-style: normal;
  }
`;
