import { useStore } from 'effector-react';
import React, { FC, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Notifications } from './components/common/Notifications';
import { effects, state } from './effects/user';
import { Main } from './pages/Main';
import { GlobalStyle } from './styles';

const App: FC = () => {
    const { token } = useStore(state.user);

    useEffect(() => {
        // load JWT token
        effects.loadToken('');
    }, []);

    if (!token) return null;

    return (
        <>
            <GlobalStyle />
            <Notifications />
            <Router>
                <Switch>
                    <Route component={Main} path="/:id" />
                </Switch>
            </Router>
        </>
    );
};

export default App;
