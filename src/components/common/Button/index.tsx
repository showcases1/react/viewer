import React, { FC } from 'react';
import { StyledButton, Text } from './styles';

export type ButtonType = 'button' | 'submit' | 'reset';

export type ButtonBgColor = 'green' | 'white';

export interface StyledButtonProps {
    type?: ButtonType;

    bgColor?: ButtonBgColor;

    disabled?: boolean;
}

interface Props extends StyledButtonProps {
    href?: string;
    onClick?: (event: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => void;
    className?: string;
    height?: string;
}

export const Button: FC<Props> = ({ href, type = 'button', bgColor, onClick, className, children, disabled, height }) =>
    href ? (
        <StyledButton
            as="a"
            bgColor={bgColor}
            className={className}
            disabled={disabled}
            height={height}
            href={href}
            target="_blank"
            onClick={onClick}
        >
            <Text>{children}</Text>
        </StyledButton>
    ) : (
        <StyledButton
            bgColor={bgColor}
            className={className}
            disabled={disabled}
            height={height}
            type={type}
            onClick={onClick}
        >
            <Text>{children}</Text>
        </StyledButton>
    );
