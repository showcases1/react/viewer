import styled from 'styled-components';
import { colors, fonts } from '../../../variables/index';
import { StyledButtonProps } from './index';

interface ButtonStyledProps extends StyledButtonProps {
    height?: string;
}

export const StyledButton = styled.button<ButtonStyledProps>`
    padding-left: 12px;
    padding-right: 12px;
    display: flex;
    align-items: center;
    justify-content: center;
    height: ${p => p.height || '25px'};
    font-family: ${fonts.sfuidSemibold};
    outline: none;
    text-decoration: none;
    color: ${colors.black};
    background-color: ${({ bgColor }) => bgColor && colors[bgColor]};
    border-radius: 100px;
    border: none;
`;

export const Text = styled.span`
    font-size: 12px;
`;
