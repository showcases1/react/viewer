import React, { FC } from 'react';
import { Progress, ProgressVideoStyled } from './styles';

interface IProgressVideo {
    duration: number;
    className?: string;
    isActiveVideo: boolean;
}

// TODO: muted для работы autoplay, разобраться как включать сразу со звуком
export const ProgressVideo: FC<IProgressVideo> = ({ duration, className, isActiveVideo }) => (
    <ProgressVideoStyled className={className}>
        <Progress duration={duration} isActiveVideo={isActiveVideo} />
    </ProgressVideoStyled>
);
