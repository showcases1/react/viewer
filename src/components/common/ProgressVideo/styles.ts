import styled, { css } from 'styled-components';
import { colors } from 'variables';

export const ProgressVideoStyled = styled.div`
    width: calc(100% - 40px);
    height: 6px;
    background-color: rgba(151, 151, 151, 0.5);
    border-radius: 5px;
    overflow: hidden;
`;

interface ProgressProps {
    duration: number;
    isActiveVideo: boolean;
}

export const Progress = styled.div<ProgressProps>`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 6px;
    background-color: ${colors.greenLight};
    border-radius: 5px;
    overflow: hidden;
    transform: translateX(-100%);

    ${p =>
        p.isActiveVideo &&
        css`
            animation: progress ${p.duration}s;
            animation-iteration-count: infinite;
        `}

    @keyframes progress {
        0% {
            transform: translateX(-100%);
        }

        100% {
            transform: translateX(0);
        }
    }
`;
