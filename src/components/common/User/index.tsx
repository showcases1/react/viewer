import React, { FC } from 'react';
import { Image, ImageWrap, Name, UserStyled } from './styles';

interface UserProps {
    username?: string;
    userImage?: string;
    className?: string;
    isVideo: boolean;
}

export const User: FC<UserProps> = ({ username, userImage, className, isVideo }) =>
    username ? (
        <UserStyled className={className} isVideo={isVideo}>
            <ImageWrap default={!userImage}>{userImage && <Image alt="user-icon" src={userImage} />}</ImageWrap>
            <Name>{username}</Name>
        </UserStyled>
    ) : null;
