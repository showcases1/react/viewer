import Stub from 'assets/img/stub.png';
import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';
import { colors } from 'variables';

export const RelatedVideoStyled = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    height: 100%;
`;

export const User = styled.div`
    width: 100%;
    height: 170px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

export const UserImage = styled.img`
    width: 100px;
    height: 100px;
    margin-bottom: 15px;
    overflow: hidden;
    border-radius: 50%;
`;

export const Title = styled.h3`
    margin: 0;
    padding-top: 10px;
    margin-bottom: 20px;
    font-size: 14px;
    text-align: center;
    color: ${colors.black};
`;

export const UserDescription = styled.p`
    margin: 0;
    margin-bottom: 5px;
    font-size: 14px;
    color: ${colors.white};
`;

export const UserInfo = styled.div`
    text-align: center;
`;

export const UserName = styled.div`
    font-size: 20px;
    text-transform: uppercase;
    color: ${colors.white};
`;

interface ContentProp {
    isItemsCreator: boolean;
}

export const Content = styled.div<ContentProp>`
    position: absolute;
    left: 0;
    bottom: 0;
    padding: 30px 15px 15px;
    padding-top: 0;
    width: 100%;
    height: 100%;
    background-color: ${colors.white};
    overflow: auto;

    &::-webkit-scrollbar {
        width: 8px;
        height: 8px;
    }

    &::-webkit-scrollbar-track {
        background-color: rgba(9, 30, 66, 0.08);
        border-radius: 6px;
    }

    &::-webkit-scrollbar-thumb {
        background-color: ${colors.gray};
        border-radius: 6px;
    }

    ${p =>
        p.isItemsCreator &&
        css`
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
            height: calc(100% - 210px);
        `}
`;

export const Wrap = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    max-width: 434px;
    margin: -10px auto 0;
`;

export const StyledLink = styled(Link)`
    position: relative;
    margin-left: 5px;
    margin-top: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
    width: calc(50% - 10px);
    overflow: hidden;
    border-radius: 13px;
    background-color: ${colors.black};
`;

export const Image = styled.img`
    width: 100%;
`;

export const Watching = styled.div`
    position: absolute;
    bottom: 6px;
    left: 6px;
    display: flex;
    align-items: center;
`;

interface IUserWrap {
    default: boolean;
}

export const UserWrap = styled.div<IUserWrap>`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 20px;
    height: 20px;
    overflow: hidden;
    border: 2px solid ${colors.white};
    background-repeat: no-repeat;
    background-size: 100%;
    border-radius: 50%;

    ${p =>
        p.default &&
        css`
            background-image: url(${Stub});
        `}
`;

export const UserImageWatch = styled.img`
    width: 100%;
    height: 100%;
`;

export const Watch = styled.div`
    margin-left: 10px;
    padding-left: 4px;
    padding-right: 4px;
    display: flex;
    align-items: center;
    height: 20px;
    background-color: rgba(0, 0, 0, 0.3);
    border-radius: 10px;
`;

export const WatchIconStyled = styled.img`
    width: 14px;
    height: 14px;
    border-radius: 50%;
`;

export const WatchText = styled.p`
    margin: 0;
    margin-left: 6px;
    font-size: 12px;
    line-height: 20px;
    color: #fff;
`;
