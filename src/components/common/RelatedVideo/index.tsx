import Stub from 'assets/img/stub.png';
import WatchIcon from 'assets/img/watch.svg';
import React, { FC } from 'react';
import {
    Content,
    Image,
    RelatedVideoStyled,
    StyledLink,
    Title,
    User,
    UserDescription,
    UserImage,
    UserImageWatch,
    UserInfo,
    UserName,
    UserWrap,
    Watch,
    WatchIconStyled,
    Watching,
    WatchText,
    Wrap
} from './styles';

interface RelatedVideoProps {
    videos: Viewer.PlaylistVideoResponse[];
    onClick: () => void;
    video: Viewer.PlaylistSingleVideoResponse;
    isItemsCreator: boolean;
}

export const RelatedVideo: FC<RelatedVideoProps> = ({ videos, onClick, video, isItemsCreator }) => (
    <RelatedVideoStyled>
        {isItemsCreator && (
            <User>
                <UserImage src={video.profileImageUrl || Stub} />
                <UserInfo>
                    <UserDescription>also by</UserDescription>
                    <UserName>{video.username}</UserName>
                </UserInfo>
            </User>
        )}
        <Content isItemsCreator={isItemsCreator}>
            <Title>More Videos</Title>
            <Wrap>
                {videos.map(item => (
                    <StyledLink key={item.id} to={`/${item.id}`} onClick={onClick}>
                        <Image src={item.streaming?.screenGrabUrl} />
                        <Watching>
                            <UserWrap default={!video.profileImageUrl}>
                                {video.profileImageUrl && (
                                    <UserImageWatch alt="user-icon" src={video.profileImageUrl} />
                                )}
                            </UserWrap>
                            <Watch>
                                <WatchIconStyled src={WatchIcon} />
                                <WatchText>{item.engagement?.views}</WatchText>
                            </Watch>
                        </Watching>
                    </StyledLink>
                ))}
            </Wrap>
        </Content>
    </RelatedVideoStyled>
);
