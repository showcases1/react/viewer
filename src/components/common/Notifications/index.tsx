import React from 'react';
import { useStore } from 'effector-react';
import { effects, state } from 'effects/notifications';
import { StyledNotifications, Notification, Header, Message, Remove, Description } from './styles';

export const Notifications = () => {
    const notifications = useStore(state.notifications);

    if (notifications.length === 0) return null;

    return (
        <StyledNotifications>
            {notifications.map(({ message, description, id }) => (
                <Notification key={id}>
                    <Header>
                        <Message>{message}</Message>
                        <Remove onClick={() => effects.removeNotification(id)} />
                    </Header>
                    <Description>{description}</Description>
                </Notification>
            ))}
        </StyledNotifications>
    );
};
