import { Video as VideoPlayer } from 'components/common/Video';
import React, { useState } from 'react';
import SwiperCore, { Scrollbar } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import SwiperClass from 'swiper/types/swiper-class';
import { effects } from '../../../effects/playlist';
SwiperCore.use([Scrollbar]);

interface SliderVideoProps {
    list: Viewer.PlaylistVideoResponse[];
    isShowInformation: boolean;
    onOpenPopup?: () => void;
    onToggleMutedVideo: () => void;
    isMuted: boolean;
}

export const SliderVideo: React.FC<SliderVideoProps> = ({
    list,
    isShowInformation,
    onOpenPopup,
    onToggleMutedVideo,
    isMuted
}) => {
    const [activeVideoId, setActiveVideoId] = useState(list[0].id);

    const handleChangeSlide = (swiper: SwiperClass) => {
        const idVideo = list[swiper.activeIndex].id || '';
        window.history.pushState('change-slide', 'WebViewer', `/${idVideo}`);

        setActiveVideoId(idVideo);
        effects.loadVideoSlideChange(idVideo);
    };

    return (
        <Swiper direction="vertical" scrollbar={{ draggable: true }} onSlideChangeTransitionEnd={handleChangeSlide}>
            {list.map(itemVideo => (
                <SwiperSlide key={itemVideo.id}>
                    <VideoPlayer
                        isActiveVideo={activeVideoId === itemVideo.id}
                        isMuted={isMuted}
                        isShowInformation={isShowInformation}
                        video={itemVideo}
                        onOpenPopup={onOpenPopup}
                        onToggleMutedVideo={onToggleMutedVideo}
                    />
                </SwiperSlide>
            ))}
        </Swiper>
    );
};
