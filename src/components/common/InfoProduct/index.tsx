import Stub from 'assets/img/stub.png';
import React, { FC, useEffect, useRef, useState } from 'react';
import { isMobile } from 'react-device-detect';
import {
    Buttons,
    ButtonStyled,
    Description,
    DescriptionWrap,
    Image,
    ImageWrap,
    InfoProductStyled,
    InfoWrap,
    Photo,
    Products
} from './styles';

interface InfoProductProps {
    quantity?: number;
    image?: string;
    description?: string;
    onClick?: () => void;
    hashTags?: string[];
    isShowInformation?: boolean;
}

export const InfoProduct: FC<InfoProductProps> = ({
    quantity,
    image,
    description,
    hashTags,
    onClick,
    isShowInformation
}) => {
    const refDescrption = useRef<HTMLParagraphElement>(null);
    const [heightDescription, setHeightDescription] = useState(0);

    useEffect(() => {
        if (refDescrption.current) {
            setHeightDescription(refDescrption.current.clientHeight);
        }
    }, []);

    const time = 3;
    const heightWrap = 66;
    const duration = (heightDescription * time) / 100;
    const durationBack = (heightDescription * time) / 400;

    const isAnimation = heightDescription > heightWrap;

    return (
        <InfoProductStyled>
            {hashTags && (
                <Buttons isMobile={isMobile}>
                    {hashTags.map((item, key) => (
                        <ButtonStyled key={key.toString()} bgColor="white">
                            {`#${item}`}
                        </ButtonStyled>
                    ))}
                </Buttons>
            )}
            {description && isShowInformation && (
                <InfoWrap>
                    <DescriptionWrap>
                        <Description
                            ref={refDescrption}
                            duration={duration}
                            durationBack={durationBack}
                            isAnimation={isAnimation}
                        >
                            {description}
                        </Description>
                    </DescriptionWrap>
                    <Photo onClick={onClick}>
                        <ImageWrap>
                            <Image src={image || Stub} />
                        </ImageWrap>
                        {quantity && <Products>{quantity > 10 ? '10+' : quantity}</Products>}
                    </Photo>
                </InfoWrap>
            )}
        </InfoProductStyled>
    );
};
