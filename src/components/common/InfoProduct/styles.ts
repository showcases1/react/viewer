import { Button } from 'components/common/Button';
import styled, { css, keyframes } from 'styled-components';
import { colors, fonts } from '../../../variables';

const transformAnimation = keyframes`
  0 {
    transform: translateY(0);
  }

  20% {
    transform: translateY(0);
  }

  80% {
    transform: translateY(calc(-100% + 66px));
  }

  90% {
    transform: translateY(calc(-100% + 66px));
  }

  100% {
    transform: translateY(0);
  }
`;

export const InfoProductStyled = styled.div`
    position: absolute;
    left: 50%;
    bottom: 10px;
    width: 100%;
    transform: translateX(-50%);
`;

export const InfoWrap = styled.div`
    display: flex;
    justify-content: space-between;
    width: calc(100% - 40px);
    margin-left: auto;
    margin-right: auto;
`;

interface ButtonsProps {
    isMobile: boolean;
}

export const Buttons = styled.div<ButtonsProps>`
    display: flex;
    overflow-x: auto;
    margin-top: -10px;
    margin-bottom: 10px;
    padding-bottom: 10px;

    ${p =>
        !p.isMobile &&
        css`
            &::-webkit-scrollbar {
                width: 8px;
                height: 8px;
            }

            &::-webkit-scrollbar-track {
                background-color: rgba(9, 30, 66, 0.08);
                border-radius: 6px;
            }

            &::-webkit-scrollbar-thumb {
                background-color: ${colors.gray};
                border-radius: 6px;
            }
        `}
`;

export const ButtonStyled = styled(Button)`
    margin-left: 10px;
    margin-top: 10px;

    &:first-child {
        margin-left: 20px;
    }

    @media (orientation: landscape) {
        height: 18px;
    }
`;

export const DescriptionWrap = styled.div`
    margin-right: 15px;
    height: 66px;
    overflow: hidden;
`;

interface IDescription {
    duration: number;
    durationBack: number;
    isAnimation: boolean;
}

export const Description = styled.p<IDescription>`
    margin: 0;
    font-family: ${fonts.sfproRegular};
    font-size: 10px;
    line-height: 18px;
    letter-spacing: 0.3125px;
    color: ${colors.white};

    ${p =>
        p.isAnimation &&
        css`
            animation: ${transformAnimation} ${p.duration}s linear infinite;
        `}
`;

export const Photo = styled.div`
    position: relative;
    flex-shrink: 0;
    width: 70px;
    height: 70px;
`;

export const ImageWrap = styled.div`
    height: 100%;
    width: 100%;
    overflow: hidden;
    border-radius: 9px;
    border: 2px solid ${colors.white};
`;

export const Image = styled.img`
    width: 100%;
    height: 100%;
`;

export const Products = styled.div`
    position: absolute;
    bottom: -2px;
    right: -2px;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 20px;
    height: 20px;
    font-family: ${fonts.sfproSemibold};
    font-size: 12px;
    color: ${colors.black};
    background-color: ${colors.white};
    border-radius: 50%;
`;
