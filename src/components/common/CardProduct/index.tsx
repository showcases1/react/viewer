import Stub from 'assets/img/stub.png';
import React, { FC } from 'react';
import { ButtonStyled, CardProductStyled, Description, Image, Info, Title, Wrap } from './styles';

interface CardProductProps {
    imageUrl?: string;
    name?: string;
    description?: string;
    hasAffiliateLink?: boolean;
    id?: string;
    userId?: string;
    videoId?: string;
    sessionId?: string;
}

export const CardProduct: FC<CardProductProps> = ({
    imageUrl,
    name,
    description,
    hasAffiliateLink,
    id,
    userId,
    videoId,
    sessionId
}) => (
    <CardProductStyled>
        <Image src={imageUrl || Stub} />
        <Info>
            <Wrap>
                {name && <Title>{name}</Title>}
                {description && <Description>{description}</Description>}
            </Wrap>
            {hasAffiliateLink && (
                <ButtonStyled bgColor="green" href={`/out/${userId}/${videoId}/${id}/${sessionId}`}>
                    Go to website
                </ButtonStyled>
            )}
        </Info>
    </CardProductStyled>
);
