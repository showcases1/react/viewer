import { Button } from 'components/common/Button';
import styled from 'styled-components';
import { fonts } from '../../../variables';

export const CardProductStyled = styled.div`
    display: flex;
    min-height: 60px;
    width: 100%;
`;

export const Image = styled.img`
    margin-right: 15px;
    width: 90px;
    height: 90px;
    flex-shrink: 0;
    border-radius: 10px;
`;

export const Info = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`;

export const Wrap = styled.div`
    margin-bottom: 10px;
`;

export const Title = styled.h4`
    margin: 0;
    font-family: ${fonts.sfuidBold};
    font-size: 15px;
`;

export const Description = styled.p`
    margin: 0;
    margin-top: 10px;
    font-family: ${fonts.sfuidBold};
    font-size: 15px;
    font-weight: bold;
    line-height: 18px;
`;

export const ButtonStyled = styled(Button)`
    align-self: flex-end;
    height: 28px;
`;
