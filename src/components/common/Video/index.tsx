import LogoImg from 'assets/img/logo.png';
import { InfoProduct } from 'components/common/InfoProduct';
import { useStore } from 'effector-react';
import Hls from 'hls.js';
import React, { FC, useEffect, useRef, useState } from 'react';
import { isAndroid, isIOS, isMobile } from 'react-device-detect';
import { effects, state } from '../../../effects/playlist';
import { effects as effectsVideo } from '../../../effects/video';
import SoundOffIcon from './sound-off.svg';
import SoundOnIcon from './sound-on.svg';
import {
    Description,
    HeaderMobile,
    IconSound,
    LinkApp,
    Logo,
    ProgressVideoStyled,
    UserStyled,
    VideoPlay,
    VideoStyled,
    Wrapper
} from './styles';

interface VideoProps {
    video: Viewer.PlaylistSingleVideoResponse;
    onOpenPopup?: () => void;
    className?: string;
    isShowInformation?: boolean;
    isActiveVideo: boolean;
    isMuted: boolean;
    onToggleMutedVideo: () => void;
}

export const Video: FC<VideoProps> = ({
    video,
    onOpenPopup,
    className,
    isShowInformation,
    isActiveVideo,
    onToggleMutedVideo,
    isMuted
}) => {
    const videoRef = useRef<HTMLVideoElement>(null);
    const [duration, setDuration] = useState(0);
    const loadedVideo = useStore(state.loadedVideo);

    const setDurationVideo = (videoPlayer: HTMLVideoElement) => {
        videoPlayer.addEventListener('loadedmetadata', () => {
            const date = new Date();
            setDuration(videoPlayer.duration);
            effectsVideo.saveVideoInformation({
                duration: videoPlayer.duration,
                time: date.getTime()
            });
            effects.setLoadedVideo(true);
        });
    };

    useEffect(() => {
        effects.setLoadedVideo(false);

        if (!videoRef.current || !video.streaming?.hlsUrl) return;

        const videoSrc = video.streaming.hlsUrl;
        const videoPlayer = videoRef.current;

        if (Hls.isSupported()) {
            const hls = new Hls();

            hls.loadSource(videoSrc);
            hls.attachMedia(videoPlayer);
            hls.on(Hls.Events.MANIFEST_PARSED, () => {
                videoPlayer.play();
                setDurationVideo(videoPlayer);
            });
        } else if (videoPlayer.canPlayType('application/vnd.apple.mpegurl')) {
            videoPlayer.src = videoSrc;
            videoPlayer.addEventListener('loadedmetadata', () => {
                videoPlayer.play();
                setDuration(videoPlayer.duration);
                effects.setLoadedVideo(true);
            });
        }
        // TODO: show notification if browser cant play HLS
    }, [video.streaming]);

    useEffect(() => {
        const videoPlayer = videoRef.current;
        if (videoPlayer) {
            if (isActiveVideo) {
                videoPlayer.play();
            } else {
                videoPlayer?.pause();
                videoPlayer.currentTime = 0;
            }
        }
    }, [isActiveVideo, isMuted]);

    const handleClick = () => {
        onToggleMutedVideo();
    };

    const getLinkApp = () => {
        if (isAndroid) {
            return 'https://play.google.com/store/apps/details?id=Viewer.tv.Viewer&hl=ru';
        }
        if (isIOS) {
            return 'https://apps.apple.com/us/app/Viewer/id1070699048';
        }

        return '/';
    };

    const quantity = video.secondaryProductIds && (video.secondaryProductIds.length + 1 || 1);

    return (
        <VideoStyled className={className} isMobile={!isMobile}>
            {loadedVideo && <ProgressVideoStyled duration={duration} isActiveVideo={isActiveVideo} />}
            {loadedVideo && isMobile && (
                <HeaderMobile>
                    <Logo src={LogoImg} />
                    <Wrapper>
                        <Description>Download the App for free</Description>
                        <LinkApp href={getLinkApp()} target="_blank">
                            Get App
                        </LinkApp>
                    </Wrapper>
                </HeaderMobile>
            )}
            {loadedVideo && <IconSound icon={isMuted ? SoundOffIcon : SoundOnIcon} />}
            {loadedVideo && !isMobile && (
                <UserStyled isVideo userImage={video.profileImageUrl} username={video.username} />
            )}
            <VideoPlay ref={videoRef} loop playsInline muted={isMuted} onClick={handleClick} />
            {loadedVideo && video.primaryProduct && (
                <InfoProduct
                    description={video.primaryProduct.description}
                    hashTags={video.hashTags}
                    image={video.primaryProduct.imageUrl}
                    isShowInformation={isShowInformation}
                    quantity={quantity}
                    onClick={onOpenPopup}
                />
            )}
        </VideoStyled>
    );
};
