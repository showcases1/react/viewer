import styled, { css } from 'styled-components';
import { colors, fonts } from 'variables';
import { ProgressVideo } from '../ProgressVideo';
import { User } from '../User';

export const VideoPlay = styled.video`
    width: 100%;
    height: 100%;
    background-color: ${colors.black};
`;

interface IconSoundProps {
    icon: string;
}

export const IconSound = styled.div<IconSoundProps>`
    position: absolute;
    top: 80px;
    left: 20px;

    background-color: rgba(51, 51, 51, 0.5);
    width: 40px;
    height: 40px;
    border-radius: 50%;

    &::before {
        position: absolute;
        top: 50%;
        left: 50%;
        width: 20px;
        height: 20px;
        content: '';
        background-image: url(${p => p.icon});
        background-repeat: no-repeat;
        background-size: 100%;
        background-position: center;
        transform: translate(-50%, -50%);
    }
`;

export const Source = styled.source``;

export const ProgressVideoStyled = styled(ProgressVideo)`
    position: absolute;
    top: 60px;
    left: 50%;
    transform: translateX(-50%);
`;

export const HeaderMobile = styled.header`
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    padding-left: 20px;
    padding-right: 20px;
    width: 100%;
    height: 60px;
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

export const Logo = styled.img`
    width: 64px;
    height: 38px;
`;

export const Wrapper = styled.div`
    display: flex;
    align-items: center;
    height: 100%;
`;

export const Description = styled.span`
    margin-right: 12px;
    font-size: 12px;
    font-family: ${fonts.sfuidSemibold};
    color: ${colors.white};
`;

export const LinkApp = styled.a`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 80px;
    height: 30px;
    font-family: ${fonts.sfuidSemibold};
    font-size: 12px;
    text-decoration: none;
    border-radius: 30px;
    background-color: ${colors.white};
    color: ${colors.black};
`;

export const UserStyled = styled(User)`
    position: absolute;
    top: 80px;
    right: 20px;
`;

interface VideoStyledProps {
    isMobile: boolean;
}

export const VideoStyled = styled.div<VideoStyledProps>`
    position: relative;
    width: 100%;
    height: 100%;

    ${p =>
        p.isMobile &&
        css`
            ${ProgressVideoStyled} {
                top: 20px;
            }

            ${IconSound} {
                top: 40px;
            }

            ${UserStyled} {
                top: 40px;
            }
        `}
`;
