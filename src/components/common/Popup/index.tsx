import React, { FC } from 'react';
import { isMobile } from 'react-device-detect';
import { BackDrop, Content, PopupStyled } from './styles';

export interface PopupProps {
    isShow: boolean;
    onClosePopup?: () => void;
    className?: string;
    height?: string;
}

export const Popup: FC<PopupProps> = ({ children, isShow, onClosePopup, className, height = '60vh' }) => (
    <PopupStyled className={className} isMobile={isMobile} isShow={isShow}>
        <BackDrop onClick={onClosePopup} />
        <Content height={height}>{children}</Content>
    </PopupStyled>
);
