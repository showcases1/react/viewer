import ImgAppStore from 'assets/img/appStore.png';
import ImgGooglePlay from 'assets/img/googlePlay.png';
import LogoImg from 'assets/img/logo.png';
import React from 'react';
import { isMobile } from 'react-device-detect';
import {
    Container,
    HeaderStyled,
    ImageAppStore,
    ImageGooglePlay,
    Link,
    Logo,
    LogoLink,
    RightContent,
    Text
} from './styles';

export const Header = () =>
    !isMobile ? (
        <HeaderStyled>
            <Container>
                <LogoLink to="/">
                    <Logo src={LogoImg} />
                </LogoLink>
                <RightContent>
                    <Text>Download the App for free</Text>
                    <Link href="https://apps.apple.com/us/app/Viewer/id1070699048" target="_blank">
                        <ImageAppStore src={ImgAppStore} />
                    </Link>
                    <Link href="https://play.google.com/store/apps/details?id=Viewer.tv.Viewer&hl=ru" target="_blank">
                        <ImageGooglePlay src={ImgGooglePlay} />
                    </Link>
                </RightContent>
            </Container>
        </HeaderStyled>
    ) : null;
