import { Popup } from 'components/common/Popup';
import { Content as ContentPopup } from 'components/common/Popup/styles';
import styled, { css } from 'styled-components';
import { colors, fonts } from 'variables';
import { User } from '../User';

interface PopupProps {
    staticState?: boolean;
}

export const Title = styled.h3`
    margin: 0;
    margin-bottom: 15px;
    font-family: ${fonts.sfuidSemibold};
    font-size: 11px;
`;

interface IContent {
    isMobile: boolean;
}

export const Content = styled.div<IContent>`
    position: relative;
    z-index: 1;
    padding: 36px;
    padding-top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: ${colors.white};
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;

    &::-webkit-scrollbar {
        width: 8px;
        height: 8px;
    }

    &::-webkit-scrollbar-track {
        background-color: rgba(9, 30, 66, 0.08);
        border-radius: 6px;
    }

    &::-webkit-scrollbar-thumb {
        background-color: ${colors.gray};
        border-radius: 6px;
    }

    & > *:not(:last-child) {
        margin-bottom: 10px;
    }

    ${p =>
        p.isMobile &&
        css`
            padding-top: 30px;
        `}
`;

export const List = styled.div``;

export const UserStyled = styled(User)`
    margin-bottom: 18px;
`;

export const PopupStyled = styled(Popup)<PopupProps>`
    ${p =>
        p.staticState &&
        css`
            position: relative;
            z-index: 1;
            opacity: 1;
            visibility: visible;
            height: 100%;

            ${ContentPopup} {
                position: relative;
                height: 100%;
                transform: translateY(0);
            }
        `}

    @media (orientation: landscape) {
        width: 100%;
    }
`;

export const CardProductPrimary = styled.div``;
