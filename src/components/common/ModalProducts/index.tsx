import { CardProduct } from 'components/common/CardProduct';
import { useStore } from 'effector-react';
import React, { FC, useEffect, useRef, useState } from 'react';
import { isMobile } from 'react-device-detect';
import { state } from '../../../effects/playlist';
import { state as stateUser } from '../../../effects/user';
import { CardProductPrimary, Content, List, PopupStyled, Title, UserStyled } from './styles';

interface ModalProductsProps {
    isShow: boolean;
    onTogglePopup: () => void;
    staticState?: boolean;
    video: Viewer.PlaylistSingleVideoResponse;
}

export const ModalProducts: FC<ModalProductsProps> = ({ isShow, onTogglePopup, staticState, video }) => {
    const { user } = useStore(stateUser.user);
    const sessionId = useStore(stateUser.sessionId);
    const loadedVideo = useStore(state.loadedVideo);
    const refPrimary = useRef<HTMLDivElement>(null);
    const [heightPrimary, setHeightPrimary] = useState(0);

    useEffect(() => {
        if (refPrimary.current) {
            const height = refPrimary.current.clientHeight;
            setHeightPrimary(height);
        }
    }, [isShow]);

    const emptySecondaryProducts =
        !video.secondaryProducts || (Array.isArray(video.secondaryProducts) && video.secondaryProducts.length === 0);

    // Indent Close button and title + padding
    const indentContent = 166;
    const quantity = video.secondaryProductIds?.length || 0;
    const height = quantity === 0 ? `${heightPrimary + indentContent}px` : '80vh';

    return (
        <PopupStyled height={height} isShow={isShow} staticState={staticState} onClosePopup={onTogglePopup}>
            {loadedVideo && (
                <Content isMobile={isMobile}>
                    {emptySecondaryProducts && !video.primaryProduct ? (
                        <Title>This video has no products</Title>
                    ) : (
                        <>
                            <UserStyled isVideo={false} userImage={video.profileImageUrl} username={video.username} />

                            <Title>In this Video</Title>
                            <List>
                                <CardProductPrimary ref={refPrimary}>
                                    <CardProduct
                                        description={video.primaryProduct?.description}
                                        hasAffiliateLink={video.primaryProduct?.hasAffiliateLink}
                                        id={video.primaryProduct?.id}
                                        imageUrl={video.primaryProduct?.imageUrl}
                                        name={video.primaryProduct?.name}
                                        sessionId={sessionId}
                                        userId={user?.userId}
                                        videoId={video.id}
                                    />
                                </CardProductPrimary>
                                {video.secondaryProducts?.map((card, index) => (
                                    <CardProduct
                                        key={index.toString()}
                                        description={card.description}
                                        hasAffiliateLink={card.hasAffiliateLink}
                                        id={card.id}
                                        imageUrl={card.imageUrl}
                                        name={card.name}
                                        sessionId={sessionId}
                                        userId={user?.userId}
                                        videoId={video.id}
                                    />
                                ))}
                            </List>
                        </>
                    )}
                </Content>
            )}
        </PopupStyled>
    );
};
