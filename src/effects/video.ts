import { createEffect, createStore } from 'effector';
import { createNotifyingEffect } from 'utils/common';
import { API } from '../services';

const registerEventVideo = createEffect({
    handler: async (data: Viewer.RegisterAggregatedEventRequest) => await API.video.get(data)
});

interface paramsVideoProp {
    id?: string;
    sessionId?: string;
}

interface informationVideoProp {
    duration: number;
    time: number;
}

// Effects
const saveParamsVideo = createEffect({
    handler: async (data: paramsVideoProp) => await data
});

const saveVideoInformation = createEffect({
    handler: async (data: informationVideoProp) => await data
});

export const loadPlaylistCreator = createNotifyingEffect({
    handler: async (data: Viewer.QueryCreatorVideosRequest) => {
        const playlist = await API.video.queryCreator(data);

        return playlist.items || [];
    }
});

// State
const oldParamsVideo = createStore<paramsVideoProp>({}).on(
    saveParamsVideo.doneData,
    (_, oldParamsVideo) => oldParamsVideo
);

export const playlistCreator = createStore({}).on(
    loadPlaylistCreator.doneData,
    (_, playlistCreator) => playlistCreator
);

export const playlistItemsCreator = createStore([] as Viewer.PlaylistVideoResponse[]).on(
    loadPlaylistCreator.doneData,
    (currentPlaylist = [], playlist: Viewer.PlaylistVideoResponse[]) => [...currentPlaylist, ...playlist]
);

const informationSeeVideo = createStore<informationVideoProp>({
    duration: 0,
    time: 0
}).on(saveVideoInformation.doneData, (_, parameters) => parameters);

const effects = { registerEventVideo, saveParamsVideo, saveVideoInformation, loadPlaylistCreator };

const state = { oldParamsVideo, informationSeeVideo, playlistCreator, playlistItemsCreator };

export { effects, state };
