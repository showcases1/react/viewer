import { createStore } from 'effector';
import { createNotifyingEffect } from 'utils/common';
import { v4 as uuid4 } from 'uuid';
import { API } from '../services';

/**
 * Effect to load JWT token for anonymous user
 */
const loadToken = createNotifyingEffect({
    handler: async (_param = '') => await API.user.createAccountAnonymous({ locale: 'en_EN', localeStandard: 'ISO639' })
});

const user = createStore<Viewer.UserJwtTokenResponse>({}).on(loadToken.doneData, (_, user) => user);

const sessionId = createStore<string>(uuid4());

const effects = { loadToken };
const state = { user, sessionId };

export { effects, state };
