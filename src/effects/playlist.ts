import { createEvent, createStore } from 'effector';
import { createNotifyingEffect } from 'utils/common';
import { effects as effectsVideo } from '../effects/video';
import { API } from '../services';

export const loadPlaylistVideo = createNotifyingEffect({
    handler: async (playerId?: string) => {
        const playlistVideo = await API.playlist.get({ playlistVideoId: playerId });

        return playlistVideo || '';
    }
});

export const loadPlaylist = createNotifyingEffect({
    handler: async (data: Viewer.PlaylistVideosRequest) => {
        const playlist = await API.playlist.query(data);

        return playlist.items || [];
    }
});

export const playlist = createStore({}).on(loadPlaylist.doneData, (_, playlist) => playlist);

export const playlistItems = createStore([] as Viewer.PlaylistVideoResponse[]).on(
    loadPlaylist.doneData,
    (currentPlaylist = [], playlist: Viewer.PlaylistVideoResponse[]) => [...currentPlaylist, ...playlist]
);

const loadVideo = createNotifyingEffect({
    handler: async (videoId: string) => await API.playlist.get({ playlistVideoId: videoId })
});

const MAX_VIDEO_COUNT = 10;

export const mainLoad = createNotifyingEffect({
    handler: async (videoId: string) => {
        const video = await loadVideo(videoId);
        await effectsVideo.loadPlaylistCreator({
            creatorId: video.ownerId,
            limit: MAX_VIDEO_COUNT,
            pageIndex: 0
        });
        await loadPlaylist({
            limit: MAX_VIDEO_COUNT,
            pageIndex: 0
        });
    }
});

const loadVideoSlideChange = createNotifyingEffect({
    handler: async (videoId: string) => await videoId
});

export const videoSlideId = createStore({}).on(loadVideoSlideChange.doneData, (_, id) => id);

const setLoadedVideo = createEvent<boolean>();

const video = createStore<Viewer.PlaylistSingleVideoResponse>({}).on(loadVideo.doneData, (_, video) => video);

const loadedVideo = createStore(false).on(setLoadedVideo, (_state, payload) => payload);

const isLoadingVideo = createStore(true)
    .on(loadVideo, () => true)
    .on(loadVideo.done, () => false)
    .on(loadVideo.fail, () => false);

export const effects = {
    loadVideo,
    loadPlaylist,
    setLoadedVideo,
    loadVideoSlideChange,
    mainLoad
};

export const state = {
    video,
    loadedVideo,
    playlist,
    playlistItems,
    isLoadingVideo,
    videoSlideId
};
