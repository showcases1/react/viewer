import { createStore } from 'effector';
import { createNotifyingEffect } from 'utils/common';
import { API } from '../services';

const loadProduct = createNotifyingEffect({
    handler: async (id: string) => await API.product.get({ id })
});

const product = createStore<Viewer.ProductResponse>({}).on(loadProduct.doneData, (_, product) => product);

const isLoadingProduct = createStore(true)
    .on(loadProduct, () => true)
    .on(loadProduct.done, () => false)
    .on(loadProduct.fail, () => false);

export const effects = {
    loadProduct
};

export const state = {
    product,
    isLoadingProduct
};
