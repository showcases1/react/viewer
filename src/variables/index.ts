export const fonts = {
    sfproSemibold: 'SFProDisplay SemiBold, sans-serif',
    sfproRegular: 'SFProDisplay Regular, sans-serif',
    sfuidSemibold: 'SFUIDisplay SemiBold, sans-serif',
    sfuidBold: 'SFUIDisplay Bold, sans-serif'
};

export const colors = {
    white: '#FFFFFF',
    black: '#000000',
    green: '#00FF44',
    greenLight: '#33FF6B',
    gray: '#979797'
} as const;

export type Colors = typeof colors;

export type Color = keyof typeof colors;

export type Fonts = typeof fonts;

export type Font = keyof typeof fonts;
