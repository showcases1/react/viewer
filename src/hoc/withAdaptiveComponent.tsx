import React from 'react';
import { isMobile } from 'react-device-detect';

const scale = 1.77777;

interface HocState {
    isShowProducts: boolean;
    isRelatedVideo: boolean;
    width: number;
    height: number;
}

export function withAdaptiveComponent(WrappedComponent: any) {
    return class extends React.Component {
        state = {
            isShowProducts: true,
            isRelatedVideo: true,
            width: 0,
            height: 0
        };

        updateSize = () => {
            const vh = window.innerHeight * 0.01;
            document.documentElement.style.setProperty('--vh', `${vh}px`);
            // Magic number is indent header and paddings
            const indentHeader = isMobile ? 0 : 138;
            const widthWindow = window.innerWidth;
            const heightWindow = window.innerHeight - indentHeader;
            const width = heightWindow / scale;
            const height = width > widthWindow ? widthWindow * scale : heightWindow;

            this.setState({
                width: width > widthWindow ? widthWindow : width,
                height,
                isShowProducts: widthWindow >= width * 3,
                isRelatedVideo: widthWindow >= width * 2
            });
        };

        componentDidMount() {
            this.updateSize();
            window.addEventListener('resize', this.updateSize);
        }

        componentWillUnmount() {
            window.removeEventListener('resize', this.updateSize);
        }

        render() {
            return <WrappedComponent {...this.props} {...this.state} />;
        }
    };
}
