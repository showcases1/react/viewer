import { createEffect, Effect } from 'effector';
import { effects } from 'effects/notifications';
/**
 * Create effect with bad response notifications
 */
interface BadResponseErrors
    extends Viewer.Error409ConflictResponse,
        Viewer.Error400BadRequest,
        Viewer.Error404NotFoundResponse {}

export function createNotifyingEffect<Params, Done>(config: {
    name?: string;
    handler?: (params: Params) => Promise<Done> | Done;
    sid?: string;
}): Effect<Params, Done> {
    const effect = createEffect(config);
    effect.fail.watch(({ error }: { error: BadResponseErrors }) => {
        if (!error.isSuccess) {
            effects.setNotification({
                message: error.message || '',
                description: ''
            });
        }
    });
    return effect;
}
