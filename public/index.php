<!doctype html>
<html lang="en">
<head>
    <link rel="icon" href="/logo.jpg" />
    <link rel="apple-touch-icon" href="/logo.jpg" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <?php
    $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s" : "") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $str = basename($url);
    if ($str) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://Viewer-dev.xc.io/playlist/get-meta');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"videoId\":\"$str\"}");
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json-patch+json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = json_decode(curl_exec($ch));
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        if ($result->title) { ?>
            <title><?php echo $result->title; ?></title>
            <meta property='og:title' content='<?php echo $result->title; ?>'>
        <?php }
        if ($result->description) { ?>
            <meta name='description' content='<?php echo $result->description; ?>'/>
        <?php }
        if ($result->thumbnailUrl) { ?>
            <meta property='og:image' content='<?php echo $result->thumbnailUrl; ?>'>
            <meta property='og:image:width' content='900'>
            <meta property='og:image:height' content='600'>
        <?php }
    } ?>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1"/>
    <meta name="theme-color" content="#000000"/>
</head>
<body>
    <div id="root"></div>
</body>
</html>
